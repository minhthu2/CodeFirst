﻿using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace CodeFirst.Models
{
    public class Logs
    {
        public Logs() {
            this.Transactionss = new HashSet<Transactions>();
            this.Reportss = new HashSet<Reports>();
        }
        [Key]
        public string ID { get; set; }
        public string LoginDate { get; set; }
        public string LoginTime { get; set;}
        public virtual ICollection<Transactions> Transactionss { get; set; } = new List<Transactions>();
        public virtual ICollection<Reports> Reportss { get; set; }
    }
}
