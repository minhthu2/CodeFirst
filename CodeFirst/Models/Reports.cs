﻿using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace CodeFirst.Models
{
    public class Reports
    {
        public Reports() {
            this.Transactionss = new HashSet<Transactions>();
            this.Logss = new HashSet<Logs>();
            this.Accountss = new HashSet<Accounts>();
        }
        [Key]
        public string ID { get; set; }
        public string ReportName { get; set; }
        public string ReportDate { get; set; }
        public virtual ICollection<Transactions> Transactionss { get; set; }
        public virtual ICollection<Logs> Logss { get; set; }
        public virtual ICollection<Accounts> Accountss { get; set; }
    }
}
