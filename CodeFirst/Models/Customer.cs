﻿using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace CodeFirst.Models
{
    public class Customer
    {
        [Key]
        public string ID { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public string Contact { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Transactions Transactionss { get; set; }
        public Accounts Accountss { get; set; }
    }
}
