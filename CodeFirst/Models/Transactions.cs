﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Transactions
    {
        
        [Key]
        public string ID { get; set; }
        public string? Name { get; set; }
        public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
        public Logs Logss {  get; set; }  



    }
}
