﻿using Microsoft.EntityFrameworkCore;

namespace CodeFirst.Models
{
    public class CodeFirstDbContext : DbContext
    {
        public CodeFirstDbContext(DbContextOptions< CodeFirstDbContext> options)
            : base(options)
        {
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employeess { get; set; }
        public DbSet<Reports> Reportss { get; set; }
        public DbSet<Transactions> Transactionss { get; set; }
        public DbSet<Logs> Logss { get; set; }
        public DbSet<Accounts> Accountss { get; set; }
    }
}

