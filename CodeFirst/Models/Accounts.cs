﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Accounts
    {
        public Accounts() { 
            this.Reportss = new HashSet<Reports>();
        }
        [Key]
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        
        public virtual ICollection<Reports> Reportss { get; set; }
        public virtual ICollection<Customer> Customers{ get; set; } = new List<Customer>();
    }
}
