﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CodeFirst.Migrations
{
    /// <inheritdoc />
    public partial class InitialModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accountss",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    AccountName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accountss", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Employeess",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Contact = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employeess", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Logss",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginDate = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LoginTime = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logss", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Reportss",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ReportName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReportDate = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reportss", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Transactionss",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactionss", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AccountsReports",
                columns: table => new
                {
                    AccountssID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ReportssID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountsReports", x => new { x.AccountssID, x.ReportssID });
                    table.ForeignKey(
                        name: "FK_AccountsReports_Accountss_AccountssID",
                        column: x => x.AccountssID,
                        principalTable: "Accountss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountsReports_Reportss_ReportssID",
                        column: x => x.ReportssID,
                        principalTable: "Reportss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LogsReports",
                columns: table => new
                {
                    LogssID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ReportssID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogsReports", x => new { x.LogssID, x.ReportssID });
                    table.ForeignKey(
                        name: "FK_LogsReports_Logss_LogssID",
                        column: x => x.LogssID,
                        principalTable: "Logss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LogsReports_Reportss_ReportssID",
                        column: x => x.ReportssID,
                        principalTable: "Reportss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Contact = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TransactionssID = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    AccountssID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Customers_Accountss_AccountssID",
                        column: x => x.AccountssID,
                        principalTable: "Accountss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Customers_Transactionss_TransactionssID",
                        column: x => x.TransactionssID,
                        principalTable: "Transactionss",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "EmployeesTransactions",
                columns: table => new
                {
                    EmployeessID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TransactionssID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesTransactions", x => new { x.EmployeessID, x.TransactionssID });
                    table.ForeignKey(
                        name: "FK_EmployeesTransactions_Employeess_EmployeessID",
                        column: x => x.EmployeessID,
                        principalTable: "Employeess",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeesTransactions_Transactionss_TransactionssID",
                        column: x => x.TransactionssID,
                        principalTable: "Transactionss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LogsTransactions",
                columns: table => new
                {
                    LogssID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TransactionssID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogsTransactions", x => new { x.LogssID, x.TransactionssID });
                    table.ForeignKey(
                        name: "FK_LogsTransactions_Logss_LogssID",
                        column: x => x.LogssID,
                        principalTable: "Logss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LogsTransactions_Transactionss_TransactionssID",
                        column: x => x.TransactionssID,
                        principalTable: "Transactionss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReportsTransactions",
                columns: table => new
                {
                    ReportssID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TransactionssID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportsTransactions", x => new { x.ReportssID, x.TransactionssID });
                    table.ForeignKey(
                        name: "FK_ReportsTransactions_Reportss_ReportssID",
                        column: x => x.ReportssID,
                        principalTable: "Reportss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReportsTransactions_Transactionss_TransactionssID",
                        column: x => x.TransactionssID,
                        principalTable: "Transactionss",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountsReports_ReportssID",
                table: "AccountsReports",
                column: "ReportssID");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_AccountssID",
                table: "Customers",
                column: "AccountssID");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_TransactionssID",
                table: "Customers",
                column: "TransactionssID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesTransactions_TransactionssID",
                table: "EmployeesTransactions",
                column: "TransactionssID");

            migrationBuilder.CreateIndex(
                name: "IX_LogsReports_ReportssID",
                table: "LogsReports",
                column: "ReportssID");

            migrationBuilder.CreateIndex(
                name: "IX_LogsTransactions_TransactionssID",
                table: "LogsTransactions",
                column: "TransactionssID");

            migrationBuilder.CreateIndex(
                name: "IX_ReportsTransactions_TransactionssID",
                table: "ReportsTransactions",
                column: "TransactionssID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountsReports");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "EmployeesTransactions");

            migrationBuilder.DropTable(
                name: "LogsReports");

            migrationBuilder.DropTable(
                name: "LogsTransactions");

            migrationBuilder.DropTable(
                name: "ReportsTransactions");

            migrationBuilder.DropTable(
                name: "Accountss");

            migrationBuilder.DropTable(
                name: "Employeess");

            migrationBuilder.DropTable(
                name: "Logss");

            migrationBuilder.DropTable(
                name: "Reportss");

            migrationBuilder.DropTable(
                name: "Transactionss");
        }
    }
}
